# Loading

## 项目介绍
本项目是基于开源项目[Loading](https://github.com/yankai-victor/Loading) 进行harmonyos化的移植和开发的。  
Loading 是一个具有多种加载视图效果的项目。  

#### 项目名称：Loading
#### 所属系列：harmonyos的第三方组件适配移植

#### 功能：一个适用于harmonyos的加载视图
#### 项目移植状态：部分移植
#### 调用差异：由于缺少动画效果支持，因此功能只实现部分
#### 原项目GitHub地址：https://github.com/yankai-victor/Loading

## 支持功能
* 进入旋转动画，点击开始，动画可以正常旋转；点击停止，可以正常缩放。
* 进入翻页动画，点击开始可以正常播放动画。且页码逐渐增加
* 进入小球摆动动画，小球可以正常运动

## 安装教程

#### 方案一  
开发者在自己的项目中添加依赖  
```
  //核心引入 
  implementation project(':lib')
```
#### 方案二
项目根目录的build.gradle中的repositories添加：
```
    buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```

module目录的build.gradle中dependencies添加：
```
implementation 'com.gitee.ts_ohos:myLoading:1.0.0'
```

## 使用说明

#### 代码使用  

##### 1.BookLoading，设置书页动画绘制。

````java
    private void addPage() {
        LayoutConfig params = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        for (int i = 0; i < PAGE_NUM; i++) {
            PageView pageView = new PageView(getContext());
            addComponent(pageView, params);
            pageView.setTag("app_name");
            pageViews.add(pageView);
        }
    }

    private void playAnim() {
        setAnim(pageViews.get(PAGE_NUM - 1), DELAYED);
        setAnim(pageViews.get(PAGE_NUM - 1), DURATION + DELAYED);
        setAnim(pageViews.get(PAGE_NUM - 2), DURATION + DELAYED * 2);

        LogUtil.error("playAnim", "pageViews");
        for (int i = PAGE_NUM - 1; i >= 0; i--) {
            setAnim(pageViews.get(i), DURATION * 3 + (PAGE_NUM - 1 - i) * DELAYED / 2);
            LogUtil.error("playAnim", "setAnim");
        }
    }
````
##### 2.RotateLoading，设置旋转动画绘制。
````java
    //设置开始动画，放大效果。
    private void startAnimator() {
        loading();
        AnimatorProperty animator = this.createAnimatorProperty();
        animator.scaleX(1f);
        animator.scaleY(1f);
        animator.setDuration(500);
        animator.start();
    }

    //设置结束动画，缩小效果。
    private void stopAnimator() {
        AnimatorProperty animator = this.createAnimatorProperty();
        animator.scaleX(0f);
        animator.scaleY(0f);
        animator.setDuration(500);
        animator.start();
    }
````

##### 3.CollisionLoadingRenderer，设置小球绘制。
````java
        for (int i = 1; i < mBallCount - 1; i++) {
            mPaint.setAlpha(MAX_ALPHA);
            canvas.drawCircle(mBallRadius * (i * 2 - 1) + mBallSideOffsets, mBallCenterY, mBallRadius, mPaint);

            RenderHelper.setRect(mOvalRect, mBallRadius * (i * 2 - 2) + mBallSideOffsets, mHeight - mOvalVerticalRadius * 2,
                    mBallRadius * (i * 2) + mBallSideOffsets, mHeight);
            mPaint.setAlpha(OVAL_ALPHA);
            canvas.drawOval(mOvalRect, mPaint);
        }
````

##### DensityUtil，通用类，设置尺寸转换。

````java
    public static float dip2px(Context context, float dpValue) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        DisplayAttributes dA = display.getRealAttributes();
        float scale = dA.scalDensity;
        return dpValue * scale;
    }
````
##### xml，引用动画组件。
````xml
    <com.victor.mylibrary.core.book.BookLoading
        ohos:id="$+id:book_loading"
        ohos:height="100vp"
        ohos:width="150vp"
        ohos:background_element="$color:transparent"
        ohos:layout_alignment="center"/>

    <com.victor.mylibrary.core.LoadingView
        ohos:id="$+id:whorl_view"
        ohos:height="200vp"
        ohos:width="200vp"
        ohos:background_element="$color:transparent"
        ohos:layout_alignment="center"
        app:loading_renderer="CollisionLoadingRenderer"/>

    <com.victor.mylibrary.core.rotate.RotateLoading
        ohos:id="$+id:rotate_loading"
        ohos:height="80vp"
        ohos:width="80vp"
        ohos:background_element="$color:transparent"
        ohos:layout_alignment="center"
        ohos:visibility="invisible"
        app:loading_speed="11"
        app:loading_width="20"/>
````

## 效果展示
<img src="./images/main_page.jpg" width="400">
<img src="./images/RotateLoading.gif">
<img src="./images/LoadingView.gif">
<img src="./images/BookLoading.gif">


## License
Copyright 2015 yankai-victor

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.