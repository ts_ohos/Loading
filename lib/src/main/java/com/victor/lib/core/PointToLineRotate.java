/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.victor.lib.core;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class PointToLineRotate extends Component implements Component.DrawTask {
    EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
    private Paint paint;
    private float pi = (float) Math.PI;
    private float startAngle = 0;
    private float angle = 0;
    private DataAnimatorValue endValue;
    private DataAnimatorValue startValue;
    private int width = 10;

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs   属性集
     */
    public PointToLineRotate(Context context, AttrSet attrs) {
        super(context, attrs);
        addDrawTask(this);
        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(width);
        paint.setColor(new Color(Color.getIntColor("#AA66CC")));
    }

    private void loading() {
        if (startValue == null) {
            startValue = makeValueAnimator(0, 360);
        } else {
            if (!startValue.isRunning()) {
                startValue.start();
            }
        }
        if (endValue == null) {
            endValue = makeEndValueAnimator(30, 60, 120, 90, 60, 30);
        } else {
            if (!endValue.isRunning()) {
                endValue.start();
            }
        }
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                loading();
                invalidate();
            }
        }, 10);
    }

    private DataAnimatorValue makeValueAnimator(float... value) {
        DataAnimatorValue valueAnimator = new DataAnimatorValue();
        valueAnimator.setStart(value[0]);
        valueAnimator.setEnd(value[1]);
        valueAnimator.setCurveType(Animator.CurveType.LINEAR);
        valueAnimator.setDuration(1000);
        valueAnimator.start();
        return valueAnimator;
    }

    private DataAnimatorValue makeEndValueAnimator(float... value) {
        DataAnimatorValue value1 = new DataAnimatorValue();
        value1.setStart(30);
        value1.setEnd(60);
        value1.setDuration(200);
        DataAnimatorValue value2 = new DataAnimatorValue();
        value2.setStart(60);
        value2.setEnd(120);
        value2.setDuration(200);
        DataAnimatorValue value3 = new DataAnimatorValue();
        value3.setStart(120);
        value3.setEnd(90);
        value3.setDuration(200);
        DataAnimatorValue value4 = new DataAnimatorValue();
        value4.setStart(90);
        value4.setEnd(60);
        value4.setDuration(200);
        DataAnimatorValue value5 = new DataAnimatorValue();
        value5.setStart(60);
        value5.setEnd(30);
        value5.setDuration(200);
        value1.setStateChangedListener(new AnimatorChangeListener(value2));
        value2.setStateChangedListener(new AnimatorChangeListener(value3));
        value3.setStateChangedListener(new AnimatorChangeListener(value4));
        value4.setStateChangedListener(new AnimatorChangeListener(value5));
        value5.setStateChangedListener(new AnimatorChangeListener(value1));
        return value1;
    }

    private float circleR;

    private AnimatorValue makeCircleData(float[] startCoordinate, float[] RCoordinate, int delay) {
        float x1 = startCoordinate[0];
        float y1 = startCoordinate[1];
        float x0 = RCoordinate[0];
        float y0 = RCoordinate[1];
        float bb;
        circleR = (float) Math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));
        float param = (float) (Math.abs(y1 - y0) / circleR);
        if (param < -1.0) {
            param = -1.0f;
        } else if (param > 1.0) {
            param = 1.0f;
        } else {
        }
        float aa = (float) Math.asin(param);
        if (x1 >= x0 && y1 >= y0) {
            bb = aa;
        } else if (x1 < x0 && y1 >= y0) {
            bb = pi - aa;
        } else if (x1 < x0 && y1 < y0) {
            bb = aa + pi;
        } else {
            bb = 2 * pi - aa;
        }
        DataAnimatorValue circleAnimator = new DataAnimatorValue();
        circleAnimator.setStart(bb);
        circleAnimator.setEnd(bb + 2 * pi);
        circleAnimator.setDuration(1500);
        circleAnimator.setCurveType(Animator.CurveType.LINEAR);
        circleAnimator.setDelay(delay);
        circleAnimator.start();
        return circleAnimator;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        RectFloat rectF = new RectFloat();
        rectF.fuse((float) (double) width / 2, (float) (double) width / 2, (float) (getWidth() - width / 2d), (float) (getHeight() - width / 2d));
        if (null == startValue) {
            loading();
        }
        angle = endValue.getAnimatedValue();
        startAngle = startValue.getAnimatedValue();
        Arc arc = new Arc(startAngle, angle, false);
        canvas.drawArc(rectF, arc, paint);

        if (endValue.isRunning() || startValue.isRunning()) {
            invalidate();
        }
    }

    class AnimatorChangeListener implements Animator.StateChangedListener {
        DataAnimatorValue nextAnimator;

        public AnimatorChangeListener(DataAnimatorValue nextAnimator) {
            this.nextAnimator = nextAnimator;
        }

        @Override
        public void onStart(Animator animator) {
        }

        @Override
        public void onStop(Animator animator) {
        }

        @Override
        public void onCancel(Animator animator) {
        }

        @Override
        public void onEnd(Animator animator) {
            nextAnimator.start();
            endValue = nextAnimator;
        }

        @Override
        public void onPause(Animator animator) {
        }

        @Override
        public void onResume(Animator animator) {
        }
    }
}
