/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.victor.lib.core;

import ohos.agp.animation.AnimatorValue;
public class DataAnimatorValue extends AnimatorValue implements AnimatorValue.ValueUpdateListener {
    float start,end,curValue;

    public void setStart(float start) {
        this.start = start;
    }

    public void setEnd(float end) {
        this.end = end;
    }

    @Override
    public void start() {
        setValueUpdateListener(this);
        super.start();
    }

    public float getAnimatedValue() {
        return curValue;
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        if (start > end) {
            curValue = (float) (start - (Math.abs((double) start - end) * v));
        } else {
            curValue = (float) (start + (Math.abs((double) end - start) * v));
        }
    }
}
