/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.victor.lib.core;

import com.victor.lib.core.circle.jump.CollisionLoadingRenderer;
import ohos.app.Context;
import ohos.utils.PlainArray;

import java.lang.reflect.Constructor;

public final class LoadingRendererFactory {
    public static final int LOADING_RENDERER_ID_MATERIAL = 0;
    public static final int LOADING_RENDERER_ID_LEVEL = 1;
    public static final int LOADING_RENDERER_ID_WHORL = 2;
    public static final int LOADING_RENDERER_ID_GEAR = 3;
    public static final int LOADING_RENDERER_ID_SWAP = 4;
    public static final int LOADING_RENDERER_ID_GUARD = 5;
    public static final int LOADING_RENDERER_ID_DANCE = 6;
    public static final int LOADING_RENDERER_ID_COLLISION = 7;
    public static final int LOADING_RENDERER_ID_DAY_NIGHT = 8;
    public static final int LOADING_RENDERER_ID_ELECTRIC_FAN = 9;
    public static final int LOADING_RENDERER_ID_FISH = 10;
    public static final int LOADING_RENDERER_ID_GHOSTS_EYE = 11;
    public static final int LOADING_RENDERER_ID_BALLOON = 12;
    public static final int LOADING_RENDERER_ID_WATER_BOTTLE = 13;
    public static final int LOADING_RENDERER_ID_CIRCLE_BROOD = 14;
    public static final int LOADING_RENDERER_ID_COOL_WAIT = 15;

    public static final String NAME_LOADING_RENDERER_ID_MATERIAL = "MaterialLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_LEVEL = "LevelLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_WHORL = "WhorlLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_GEAR = "GearLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_SWAP = "SwapLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_GUARD = "GuardLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_DANCE = "DanceLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_COLLISION = "CollisionLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_DAY_NIGHT = "DayNightLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_ELECTRIC_FAN = "ElectricFanLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_FISH = "FishLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_GHOSTS_EYE = "GhostsEyeLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_BALLOON = "BalloonLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_WATER_BOTTLE = "WaterBottleLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_CIRCLE_BROOD = "CircleBroodLoadingRenderer";
    public static final String NAME_LOADING_RENDERER_ID_COOL_WAIT = "CoolWaitLoadingRenderer";

    private static final PlainArray<Class<? extends LoadingRenderer>> LOADING_RENDERERS = new PlainArray<>();

    static {
        //circle jump
        LOADING_RENDERERS.put(LOADING_RENDERER_ID_COLLISION, CollisionLoadingRenderer.class);
    }

    private LoadingRendererFactory() {
    }

    public static LoadingRenderer createLoadingRenderer(Context context, int loadingRendererId) throws Exception {
        Class<?> loadingRendererClazz = LOADING_RENDERERS.get(loadingRendererId).get();
        Constructor<?>[] constructors = loadingRendererClazz.getDeclaredConstructors();
        for (Constructor<?> constructor : constructors) {
            Class<?>[] parameterTypes = constructor.getParameterTypes();
            if (parameterTypes != null
                    && parameterTypes.length == 1
                    && parameterTypes[0].equals(Context.class)) {
                constructor.setAccessible(true);
                return (LoadingRenderer) constructor.newInstance(context);
            }
        }

        throw new InstantiationException();
    }

    public static int parseRendererNameToId(String name) {
        if (name == null) {
            return LOADING_RENDERER_ID_MATERIAL;
        }
        switch (name) {
            case NAME_LOADING_RENDERER_ID_MATERIAL:
                return LOADING_RENDERER_ID_MATERIAL;
            case NAME_LOADING_RENDERER_ID_LEVEL:
                return LOADING_RENDERER_ID_LEVEL;
            case NAME_LOADING_RENDERER_ID_WHORL:
                return LOADING_RENDERER_ID_WHORL;
            case NAME_LOADING_RENDERER_ID_GEAR:
                return LOADING_RENDERER_ID_GEAR;
            case NAME_LOADING_RENDERER_ID_SWAP:
                return LOADING_RENDERER_ID_SWAP;
            case NAME_LOADING_RENDERER_ID_GUARD:
                return LOADING_RENDERER_ID_GUARD;
            case NAME_LOADING_RENDERER_ID_DANCE:
                return LOADING_RENDERER_ID_DANCE;
            case NAME_LOADING_RENDERER_ID_COLLISION:
                return LOADING_RENDERER_ID_COLLISION;
            case NAME_LOADING_RENDERER_ID_DAY_NIGHT:
                return LOADING_RENDERER_ID_DAY_NIGHT;
            case NAME_LOADING_RENDERER_ID_ELECTRIC_FAN:
                return LOADING_RENDERER_ID_ELECTRIC_FAN;
            case NAME_LOADING_RENDERER_ID_FISH:
                return LOADING_RENDERER_ID_FISH;
            case NAME_LOADING_RENDERER_ID_GHOSTS_EYE:
                return LOADING_RENDERER_ID_GHOSTS_EYE;
            case NAME_LOADING_RENDERER_ID_BALLOON:
                return LOADING_RENDERER_ID_BALLOON;
            case NAME_LOADING_RENDERER_ID_WATER_BOTTLE:
                return LOADING_RENDERER_ID_WATER_BOTTLE;
            case NAME_LOADING_RENDERER_ID_CIRCLE_BROOD:
                return LOADING_RENDERER_ID_CIRCLE_BROOD;
            case NAME_LOADING_RENDERER_ID_COOL_WAIT:
                return LOADING_RENDERER_ID_COOL_WAIT;
            default:
                return LOADING_RENDERER_ID_MATERIAL;
        }
    }
}
