/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.victor.lib.core.util;

import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

public class RenderHelper {

    public static void setRect(Rect src, Rect dst) {
        dst.set(src.left, src.top, src.right, src.bottom);
    }

    public static void setRect(Rect src, RectFloat dst) {
        dst.left = src.left;
        dst.top = src.top;
        dst.right = src.right;
        dst.bottom = src.bottom;
    }

    public static void setRect(RectFloat src, RectFloat dst) {
        dst.left = src.left;
        dst.top = src.top;
        dst.right = src.right;
        dst.bottom = src.bottom;
    }

    public static void setRect(RectFloat rect, float left, float top, float right, float bottom) {
        rect.left = left;
        rect.top = top;
        rect.right = right;
        rect.bottom = bottom;
    }

    public static int alpha(Color color) {
        return (color.getValue() >> 24) & 0xff;
    }

    public static int red(Color color) {
        return (color.getValue() >> 16) & 0xff;
    }

    public static int green(Color color) {
        return (color.getValue() >> 8) & 0xff;
    }

    public static int blue(Color color) {
        return color.getValue() & 0xff;
    }

}
