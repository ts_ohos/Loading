package com.victor.lib.core.book;

import com.victor.lib.core.LogUtil;
import com.victor.lib.core.ResourceTable;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorScatter;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class BookLoading extends StackLayout {

    private static final long DURATION = 1000;

    private static final int PAGE_NUM = 5;

    private static final int DELAYED = 200;

    private ArrayList<PageView> pageViews;

    private boolean isStart;
    private BookHandler bookHandler;

    public BookLoading(Context context) {
        super(context);
        initView(context);
    }

    public BookLoading(Context context, AttrSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public BookLoading(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, String.valueOf(defStyleAttr));
        initView(context);
    }

    private void initView(Context context) {
        Component root = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_book_loading, this, true);
        pageViews = new ArrayList<>();
        bookHandler = new BookHandler(this);
        addPage();
    }

    private void addPage() {
        LayoutConfig params = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        for (int i = 0; i < PAGE_NUM; i++) {
            PageView pageView = new PageView(getContext());
            addComponent(pageView, params);
            pageView.setTag("app_name");
            pageViews.add(pageView);
        }
    }


    private void playAnim() {
        setAnim(pageViews.get(PAGE_NUM - 1), DELAYED);
        setAnim(pageViews.get(PAGE_NUM - 1), DURATION + DELAYED);
        setAnim(pageViews.get(PAGE_NUM - 2), DURATION + DELAYED * 2);

        LogUtil.error("playAnim", "pageViews");
        for (int i = PAGE_NUM - 1; i >= 0; i--) {
            setAnim(pageViews.get(i), DURATION * 3 + (PAGE_NUM - 1 - i) * DELAYED / 2);
            LogUtil.error("playAnim", "setAnim");
        }
    }


    private void setAnim(Component view, long delay) {
        //创建数值动画对象
        AnimatorScatter scatter = AnimatorScatter.getInstance(getContext());
        Animator animator = scatter.parse(ResourceTable.Animation_animator_value);
        AnimatorValue animatorValue = (AnimatorValue) animator;
        //循环次数
        animatorValue.setLoopedCount(AnimatorValue.INFINITE);
        animatorValue.setDelay(delay);
        //动画的播放类型
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
        //设置动画过程
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            boolean change = false;

            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                view.setContentPosition((int) (-800 * value), view.getContentPositionY());
                change = true;
//                    view.moveChildToFront(view.getComponentAt(v));
            }
        });

        //开始启动动画
        animatorValue.start();
    }


    public void start() {
        isStart = true;
//        bookHandler.getEventRunner().run();
        playAnim();
    }

    public void stop() {
        isStart = false;
//        bookHandler.removeTask(null);
//        bookHandler.removeAllEvent();
    }

    public boolean isStart() {
        return isStart;
    }

    static class BookHandler extends EventHandler {
        private WeakReference<BookLoading> weakReference;

        public BookHandler(BookLoading bookLoading) {
            super(EventRunner.getMainEventRunner());
            weakReference = new WeakReference<>(bookLoading);
        }

        @Override
        public void processEvent(InnerEvent msg) {
            BookLoading bookLoading = weakReference.get();
            if (null == bookLoading)
                return;
            bookLoading.playAnim();

            InnerEvent message = InnerEvent.get();
            sendEvent(message, DURATION * 5);
        }
    }

}
