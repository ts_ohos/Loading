/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.victor.lib.core.circle.jump;

import com.victor.lib.core.DensityUtil;
import com.victor.lib.core.LoadingRenderer;
import com.victor.lib.core.compat.AccelerateInterpolator;
import com.victor.lib.core.compat.DecelerateInterpolator;
import com.victor.lib.core.compat.Interpolator;
import com.victor.lib.core.util.RenderHelper;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class CollisionLoadingRenderer extends LoadingRenderer {
    private static final Interpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    private static final Interpolator DECELERATE_INTERPOLATOR = new DecelerateInterpolator();

    private static final float MAX_ALPHA = 1;
    private static final float OVAL_ALPHA = 0.25f;

    private static final int DEFAULT_BALL_COUNT = 5;

    private static final float DEFAULT_OVAL_HEIGHT = 1.5f;
    private static final float DEFAULT_BALL_RADIUS = 7.5f;
    private static final float DEFAULT_WIDTH = 15.0f * 11;
    private static final float DEFAULT_HEIGHT = 15.0f * 4;

    private static final float START_LEFT_DURATION_OFFSET = 0.25f;
    private static final float START_RIGHT_DURATION_OFFSET = 0.5f;
    private static final float END_RIGHT_DURATION_OFFSET = 0.75f;
    private static final float END_LEFT_DURATION_OFFSET = 1.0f;

    //渐变颜色
    private static final Color[] DEFAULT_COLORS = new Color[]{
            new Color(0xFF28435D), new Color(0xFFC32720)
    };

    private static final float[] DEFAULT_POSITIONS = new float[]{
            0.0f, 1.0f
    };

    private final Paint mPaint = new Paint();
    private final RectFloat mOvalRect = new RectFloat();

    private Color[] mColors;
    private float[] mPositions;

    private float mOvalVerticalRadius;

    private float mBallRadius;
    private float mBallCenterY;
    private float mBallSideOffsets;
    private float mBallMoveXOffsets;
    private float mBallQuadCoefficient;

    private float mLeftBallMoveXOffsets;
    private float mLeftBallMoveYOffsets;
    private float mRightBallMoveXOffsets;
    private float mRightBallMoveYOffsets;

    private float mLeftOvalShapeRate;
    private float mRightOvalShapeRate;

    private int mBallCount;

    private CollisionLoadingRenderer(Context context) {
        super(context);
        init(context);
        adjustParams();
        setupPaint();
    }

    private void init(Context context) {
        mPaint.setAntiAlias(true);

        mBallRadius = DensityUtil.dip2px(context, DEFAULT_BALL_RADIUS);
        mWidth = DensityUtil.dip2px(context, DEFAULT_WIDTH);
        mHeight = DensityUtil.dip2px(context, DEFAULT_HEIGHT);
        //圆球地步垂直半径
        mOvalVerticalRadius = DensityUtil.dip2px(context, DEFAULT_OVAL_HEIGHT);
        mOvalVerticalRadius = 0;

        mColors = DEFAULT_COLORS;
        mPositions = DEFAULT_POSITIONS;

        mBallCount = DEFAULT_BALL_COUNT;

        //mBallMoveYOffsets = mBallQuadCoefficient * mBallMoveXOffsets ^ 2
        // ==> if mBallMoveYOffsets == mBallMoveXOffsets
        // ==> mBallQuadCoefficient = 1.0f / mBallMoveXOffsets;
        mBallMoveXOffsets = 1.5f * (2 * mBallRadius);
        mBallQuadCoefficient = 1.0f / mBallMoveXOffsets;
    }

    private void adjustParams() {
        mBallCenterY = mHeight / 2.0f;
        mBallSideOffsets = (mWidth - mBallRadius * 2.0f * (mBallCount - 2)) / 2;
    }

    private void setupPaint() {
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setColor(Color.WHITE);
        //设置渐变颜色
//        mPaint.setShader(new LinearShader(new Point[]{new Point(mBallSideOffsets, 0), new Point(mWidth - mBallSideOffsets, 0)},
//                mPositions, mColors, Shader.TileMode.CLAMP_TILEMODE), Paint.ShaderType.LINEAR_SHADER);
    }

    @Override
    protected void draw(Canvas canvas) {
        int saveCount = canvas.save();

        for (int i = 1; i < mBallCount - 1; i++) {
            mPaint.setAlpha(MAX_ALPHA);
            canvas.drawCircle(mBallRadius * (i * 2 - 1) + mBallSideOffsets, mBallCenterY, mBallRadius, mPaint);

            RenderHelper.setRect(mOvalRect, mBallRadius * (i * 2 - 2) + mBallSideOffsets, mHeight - mOvalVerticalRadius * 2,
                    mBallRadius * (i * 2) + mBallSideOffsets, mHeight);
            mPaint.setAlpha(OVAL_ALPHA);
            canvas.drawOval(mOvalRect, mPaint);
        }

        //draw the first ball
        mPaint.setAlpha(MAX_ALPHA);
        canvas.drawCircle(mBallSideOffsets - mBallRadius - mLeftBallMoveXOffsets,
                mBallCenterY - mLeftBallMoveYOffsets, mBallRadius, mPaint);

        RenderHelper.setRect(mOvalRect, mBallSideOffsets - mBallRadius - mBallRadius * mLeftOvalShapeRate - mLeftBallMoveXOffsets,
                mHeight - mOvalVerticalRadius - mOvalVerticalRadius * mLeftOvalShapeRate,
                mBallSideOffsets - mBallRadius + mBallRadius * mLeftOvalShapeRate - mLeftBallMoveXOffsets,
                mHeight - mOvalVerticalRadius + mOvalVerticalRadius * mLeftOvalShapeRate);
        mPaint.setAlpha(OVAL_ALPHA);
        canvas.drawOval(mOvalRect, mPaint);

        //draw the last ball
        mPaint.setAlpha(MAX_ALPHA);
        canvas.drawCircle(mBallRadius * (mBallCount * 2 - 3) + mBallSideOffsets + mRightBallMoveXOffsets,
                mBallCenterY - mRightBallMoveYOffsets, mBallRadius, mPaint);

        RenderHelper.setRect(mOvalRect, mBallRadius * (mBallCount * 2 - 3) - mBallRadius * mRightOvalShapeRate + mBallSideOffsets + mRightBallMoveXOffsets,
                mHeight - mOvalVerticalRadius - mOvalVerticalRadius * mRightOvalShapeRate,
                mBallRadius * (mBallCount * 2 - 3) + mBallRadius * mRightOvalShapeRate + mBallSideOffsets + mRightBallMoveXOffsets,
                mHeight - mOvalVerticalRadius + mOvalVerticalRadius * mRightOvalShapeRate);
        mPaint.setAlpha(OVAL_ALPHA);
        canvas.drawOval(mOvalRect, mPaint);

        canvas.restoreToCount(saveCount);
    }

    @Override
    protected void computeRender(float renderProgress) {
        // Moving the left ball to the left sides only occurs in the first 25% of a jump animation
        if (renderProgress <= START_LEFT_DURATION_OFFSET) {
            float startLeftOffsetProgress = renderProgress / START_LEFT_DURATION_OFFSET;
            computeLeftBallMoveOffsets(DECELERATE_INTERPOLATOR.getInterpolation(startLeftOffsetProgress));
            return;
        }

        // Moving the left ball to the origin location only occurs between 25% and 50% of a jump ring animation
        if (renderProgress <= START_RIGHT_DURATION_OFFSET) {
            float startRightOffsetProgress = (renderProgress - START_LEFT_DURATION_OFFSET) / (START_RIGHT_DURATION_OFFSET - START_LEFT_DURATION_OFFSET);
            computeLeftBallMoveOffsets(ACCELERATE_INTERPOLATOR.getInterpolation(1.0f - startRightOffsetProgress));
            return;
        }

        // Moving the right ball to the right sides only occurs between 50% and 75% of a jump animation
        if (renderProgress <= END_RIGHT_DURATION_OFFSET) {
            float endRightOffsetProgress = (renderProgress - START_RIGHT_DURATION_OFFSET) / (END_RIGHT_DURATION_OFFSET - START_RIGHT_DURATION_OFFSET);
            computeRightBallMoveOffsets(DECELERATE_INTERPOLATOR.getInterpolation(endRightOffsetProgress));
            return;
        }

        // Moving the right ball to the origin location only occurs after 75% of a jump animation
        if (renderProgress <= END_LEFT_DURATION_OFFSET) {
            float endRightOffsetProgress = (renderProgress - END_RIGHT_DURATION_OFFSET) / (END_LEFT_DURATION_OFFSET - END_RIGHT_DURATION_OFFSET);
            computeRightBallMoveOffsets(ACCELERATE_INTERPOLATOR.getInterpolation(1 - endRightOffsetProgress));
            return;
        }

    }

    private void computeLeftBallMoveOffsets(float progress) {
        mRightBallMoveXOffsets = 0.0f;
        mRightBallMoveYOffsets = 0.0f;

//        mLeftOvalShapeRate = 1.0f - progress;
        mLeftOvalShapeRate = 0;
        mLeftBallMoveXOffsets = mBallMoveXOffsets * progress;
        mLeftBallMoveYOffsets = (float) (Math.pow(mLeftBallMoveXOffsets, 2) * mBallQuadCoefficient);
    }

    private void computeRightBallMoveOffsets(float progress) {
        mLeftBallMoveXOffsets = 0.0f;
        mLeftBallMoveYOffsets = 0.0f;

//        mRightOvalShapeRate = 1.0f - progress;
        mRightOvalShapeRate = 0;
        mRightBallMoveXOffsets = mBallMoveXOffsets * progress;
        mRightBallMoveYOffsets = (float) (Math.pow(mRightBallMoveXOffsets, 2) * mBallQuadCoefficient);
    }

    @Override
    protected void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    @Override
    protected void setColorFilter(ColorFilter cf) {
        mPaint.setColorFilter(cf);
    }

    @Override
    protected void reset() {
    }

    private void apply(Builder builder) {
        this.mWidth = builder.mWidth > 0 ? builder.mWidth : this.mWidth;
        this.mHeight = builder.mHeight > 0 ? builder.mHeight : this.mHeight;

        this.mOvalVerticalRadius = builder.mOvalVerticalRadius > 0 ? builder.mOvalVerticalRadius : this.mOvalVerticalRadius;
        this.mBallRadius = builder.mBallRadius > 0 ? builder.mBallRadius : this.mBallRadius;
        this.mBallMoveXOffsets = builder.mBallMoveXOffsets > 0 ? builder.mBallMoveXOffsets : this.mBallMoveXOffsets;
        this.mBallQuadCoefficient = builder.mBallQuadCoefficient > 0 ? builder.mBallQuadCoefficient : this.mBallQuadCoefficient;
        this.mBallCount = builder.mBallCount > 0 ? builder.mBallCount : this.mBallCount;

        this.mDuration = builder.mDuration > 0 ? builder.mDuration : this.mDuration;

        this.mColors = builder.mColors != null ? builder.mColors : this.mColors;

        adjustParams();
        setupPaint();
    }

    public static class Builder {
        private Context mContext;

        private int mWidth;
        private int mHeight;

        private float mOvalVerticalRadius;

        private int mBallCount;
        private float mBallRadius;
        private float mBallMoveXOffsets;
        private float mBallQuadCoefficient;

        private int mDuration;

        private Color[] mColors;

        public Builder(Context mContext) {
            this.mContext = mContext;
        }

        public Builder setWidth(int width) {
            this.mWidth = width;
            return this;
        }

        public Builder setHeight(int height) {
            this.mHeight = height;
            return this;
        }

        public Builder setOvalVerticalRadius(int ovalVerticalRadius) {
            this.mOvalVerticalRadius = ovalVerticalRadius;
            return this;
        }

        public Builder setBallRadius(int ballRadius) {
            this.mBallRadius = ballRadius;
            return this;
        }

        public Builder setBallMoveXOffsets(int ballMoveXOffsets) {
            this.mBallMoveXOffsets = ballMoveXOffsets;
            return this;
        }

        public Builder setBallQuadCoefficient(int ballQuadCoefficient) {
            this.mBallQuadCoefficient = ballQuadCoefficient;
            return this;
        }

        public Builder setBallCount(int ballCount) {
            this.mBallCount = ballCount;
            return this;
        }

        public Builder setColors(Color[] colors) {
            this.mColors = colors;
            return this;
        }

        public Builder setDuration(int duration) {
            this.mDuration = duration;
            return this;
        }

        public CollisionLoadingRenderer build() {
            CollisionLoadingRenderer loadingRenderer = new CollisionLoadingRenderer(mContext);
            loadingRenderer.apply(this);
            return loadingRenderer;
        }
    }
}
