package com.victor.lib.core.book;

import ohos.agp.colors.RgbPalette;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.app.Context;


public class PageView extends Component implements Component.DrawTask {

    private Paint paint;
    private Path path;

    private int width = 450;
    private int height = 300;
    private float padding = 45;
    private int border = 30;

    public PageView(Context context) {
        super(context);
        initView();
        addDrawTask(this);
    }

    public PageView(Context context, AttrSet attrs) {
        super(context, attrs);
        initView();
        addDrawTask(this);
    }

    public PageView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        addDrawTask(this);
    }

    private void initView() {
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(10);
        path = new Path();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        paint.setColor(new Color(RgbPalette.parse("#ff33b5e5")));
        paint.setStyle(Paint.Style.STROKE_STYLE);
        float offset = border / 4;
        path.moveTo(width / 2, padding + offset);
        path.lineTo(width - padding - offset, padding + offset);
        path.lineTo(width - padding - offset, height - padding - offset);
        path.lineTo(width / 2, height - padding - offset);
        canvas.drawPath(path, paint);

        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL_STYLE);
        offset = border / 2;
        canvas.drawRect(width / 2, padding + offset, width - padding - offset, height - padding - offset, paint);
    }

    public Rect clipSquare(Rect rect, int bound) {
        int w = rect.getWidth();
        int h = rect.getHeight();
        int min = Math.min(w, h);
        int cx = rect.getCenterX();
        int cy = rect.getCenterY();
        int r = min / 2;
        return new Rect(
                cx - r + bound,
                cy - r + bound,
                cx + r - bound,
                cy + r - bound
        );
    }
}
