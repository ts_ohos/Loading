package com.victor.lib.core.book;

import ohos.agp.colors.RgbPalette;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class BookView extends Component implements Component.DrawTask {

    private Paint paint;
    private int width = 360;
    private int height = 210;

    public BookView(Context context) {
        super(context);
        initView();
        addDrawTask(this);
    }

    public BookView(Context context, AttrSet attrs) {
        super(context, attrs);
        initView();
        addDrawTask(this);
    }

    public BookView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        addDrawTask(this);
    }

    private void initView() {
        paint = new Paint();
        paint.setColor(new Color(RgbPalette.parse("#ff33b5e5")));

        paint.setStrokeWidth(10);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE_STYLE);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawRect(0, 0, width, height, paint);
    }
}
