package com.victor.lib.core.newton;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class CradleBall extends Component implements Component.EstimateSizeListener, Component.DrawTask {

    private int width;
    private int height;

    private Paint paint;

    private int loadingColor = Color.WHITE.getValue();

    public CradleBall(Context context) {
        super(context);
        initView(null);
    }

    public CradleBall(Context context, AttrSet attrs) {
        super(context, attrs);
        initView(attrs);
    }

    public CradleBall(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(attrs);
    }

    private void initView(AttrSet attrs) {
        if (null != attrs) {
            if (attrs.getAttr("cradle_ball_color").isPresent()) {
                loadingColor = attrs.getAttr("cradle_ball_color").get().getDimensionValue();
            } else {
                loadingColor = Color.WHITE.getValue();
            }
        }
        paint = new Paint();
        paint.setColor(new Color(loadingColor));
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setAntiAlias(true);
    }


    public void setLoadingColor(int color) {
        loadingColor = color;
        paint.setColor(new Color(color));
        invalidate();
    }

    public int getLoadingColor() {
        return loadingColor;
    }

    @Override
    public boolean onEstimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        width = widthEstimatedConfig;
        height = heightEstimatedConfig;
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawCircle(width / 2, height / 2, width / 2, paint);
    }
}
