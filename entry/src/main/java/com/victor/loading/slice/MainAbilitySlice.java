package com.victor.loading.slice;

import com.victor.loading.BookActivity;
import com.victor.loading.NewtonCradleActivity;
import com.victor.loading.ResourceTable;
import com.victor.loading.RotateActivity;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice {
    Button RotateLoading, BookLoading, NewtonCradleLoading;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        RotateLoading = (Button) findComponentById(ResourceTable.Id_rotate_page);
        BookLoading = (Button) findComponentById(ResourceTable.Id_book_page);
        NewtonCradleLoading = (Button) findComponentById(ResourceTable.Id_newton_page);

        RotateLoading.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new RotateActivity(), new Intent());
            }
        });

        BookLoading.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new BookActivity(), new Intent());
            }
        });

        NewtonCradleLoading.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new NewtonCradleActivity(), new Intent());
            }
        });

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
