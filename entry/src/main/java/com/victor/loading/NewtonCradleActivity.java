package com.victor.loading;

import com.victor.lib.core.LoadingView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class NewtonCradleActivity extends AbilitySlice {
    private Button button;
    LoadingView loadingView;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_activity_newton_cradle);
        button = (Button) findComponentById(ResourceTable.Id_stop_button);
        loadingView = (LoadingView) findComponentById(ResourceTable.Id_whorl_view);

        loadingView.stopAnimation();
        button.setText("start");

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (button.getText().equals("start")) {
                    loadingView.startAnimation();
                    button.setText("stop");
                } else {
                    loadingView.stopAnimation();
                    button.setText("start");
                }
            }
        });
    }
}
