package com.victor.loading;

import com.victor.lib.core.rotate.RotateLoading;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class RotateActivity extends AbilitySlice {
    private Button button;
    RotateLoading rotateLoading;


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_activity_rotate);
        button = (Button) findComponentById(ResourceTable.Id_button);

        rotateLoading = (RotateLoading) findComponentById(ResourceTable.Id_rotate_loading);
        button.setText("start");
        rotateLoading.setVisibility(Component.INVISIBLE);
        rotateLoading.stop();

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (button.getText().equals("start")) {
                    rotateLoading.setVisibility(Component.VISIBLE);
                    rotateLoading.start();
                    button.setText("stop");
                } else {
                    rotateLoading.stop();
                    button.setText("start");
                }

            }
        });
    }
}
