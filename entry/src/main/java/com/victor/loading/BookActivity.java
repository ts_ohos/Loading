package com.victor.loading;

import com.victor.lib.core.book.BookLoading;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class BookActivity extends AbilitySlice {
    private Button button;
    BookLoading bookLoading;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_activity_book);

        button = (Button) findComponentById(ResourceTable.Id_button);
        bookLoading = (BookLoading) findComponentById(ResourceTable.Id_book_loading);


        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (button.getText().equals("start")) {
                    bookLoading.start();
                    button.setText("stop");
                } else {
                    button.setText("start");
                    bookLoading.stop();
                }

            }
        });

    }
}
